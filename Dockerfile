FROM openjdk:19-alpine
ADD build/libs/*.jar manager_eureka.jar
ENTRYPOINT ["java", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/manager_eureka.jar"]
EXPOSE 8761
EXPOSE 5005